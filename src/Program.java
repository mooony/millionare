import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) throws IOException {
        List<Question> questionList = new ArrayList<>();
        List<Integer> usedQuestionIndex = new ArrayList<>();
        List<String> strings = Files.readAllLines(Paths.get("/home/jooj/Рабочий стол/projects IDEA/MillionareGame/out/production/MillionareGame/questions.txt"));
        Question question = null;
        String state = "stat";
        System.out.println("Вопрос:");
        for (String line : strings) {
            if (line.startsWith("question")) {
                state = "question";
                if (question != null) {
                    questionList.add(question);
                }
                question = new Question();
                question.setAnswerList(new ArrayList<>());
                continue;
            }
            if (line.startsWith("answers")) {
                state = "answers";
                continue;
            }
            switch (state) {
                case "question": {
                    question.setQuestion(line.trim() + "\n");
                    break;
                }
                case "answers": {
                    String trim = line.trim();
                    if (trim.startsWith("*")) {
                        trim = trim.substring(1);
                        question.setCorrectAnswer(trim);
                    }
                    question.getAnswerList().add(trim);
                    break;
                }
            }
        }
        questionList.add(question);
        while (true) {
            int questionIndex;
            while (true) {
                questionIndex = getRandomNumber(0, questionList.size());
                if(!usedQuestionIndex.contains(questionIndex)) {
                    usedQuestionIndex.add(questionIndex);
                    break;
                }
            }
            Question q = questionList.get(questionIndex);
            System.out.println(q.getQuestion());
            for (int i = 0; i < q.getAnswerList().size(); i++) {
                System.out.println(i + ") " + q.getAnswerList().get(i));
            }
            Scanner scanner = new Scanner(System.in);
            System.out.println("Выберите правильный ответ: ");
            String userAnswer = scanner.nextLine();
            if (userAnswer.equalsIgnoreCase("Выйти")) {
                break;
            }
            int selectedIndex = Integer.parseInt(userAnswer);
            String selectedAnswer = q.getAnswerList().get(selectedIndex);
            if (selectedAnswer.equalsIgnoreCase(q.getCorrectAnswer())) {
                System.out.println("Ты молодец это правильный ответ");
            } else {
                System.out.println("Не правильно. Ответ был: " + q.getCorrectAnswer() + ". Ты проебал сотку.");
            }
        }
        System.out.println("Приходи ещё!");
    }

    public static int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }
}
